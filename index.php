<?php

use App\Routing\Router;

require_once('vendor/autoload.php');

$router = new Router();

// chaque route retourne un callable
$routes = require_once __DIR__ . '/src/routes.php';
$routes($router);
echo $router->dispatch();
