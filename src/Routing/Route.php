<?php

namespace App\Routing;

class Route 
{

    protected string $method;
    protected string $path;
    protected $handler;

    public function __construct(string $method, string $path, callable $handler)
    {
        $this->method = $method;
        $this->path = $path;
        $this->handler = $handler;
    }

    public function path() : string
    {
        return $this->path;
    }

    public function method(): string{
        return $this->path;
    }

    public function dispatch()
    {
        return call_user_func($this->handler);
    }

    /**
     * On vérifie si la méthode et le chemin corespondent bien
     * @return bool
     */
    public function matches(string $method, string $path) : bool
    {
        return $this->method === $method and $this->path === $path;
    }
}