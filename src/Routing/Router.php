<?php

namespace App\Routing;

use App\Routing\Route;
use Throwable;

class Router {

    /**
     * @var array qui va stocker toutes les routes possible d'accès
     */
    protected array $routes;

    /**
     * @var array qui va stocker les erreurs du gestionnaire de route
     */
    protected array $errorHandler = [];

    public function addRoute(string $method, string $path, callable $handler) : Route
    {
        return $this->routes[] = new Route($method, $path, $handler);
    }

    public function errorHandler(int $code, callable $handler)
    {
        $this->errorHandlers[$code] = $handler;
    }

    /**
     * 
     * @return array of $paths
     */
    private function paths() : array
    {
        $paths = [];
        foreach($this->routes as $route){
            $paths[] = $route->path();
        }
        return $paths;
    }

    private function match(string $method, string $path) 
    {
        foreach($this->routes as $route){
            if($route->matches($method, $path)){
                return $route;
            }
        }
    }

    public function dispatch()
    {
        $paths = $this->paths();

        // On définit les variable method et uri
        $requestMethod = $_SERVER['REQUEST_METHOD'] ?? 'GET';
        $requestPath = $_SERVER['REQUEST_URI'] ?? '/';

        // est-ce qu'on a un matching entre la method et le chemin ?
        $matching = $this->match($requestMethod, $requestPath);

        if($matching){
            try{
                return $matching->dispatch();
            } catch (Throwable $e) {
                return $this->dispatchError();
            }
        }

        if(in_array($requestPath, $paths)) {
            return $this->dispatchNotAllowed();
        }
        return $this->dispatchNotFound();
    }

    public function dispatchNotAllowed()
    {
        $this->errorHandlers[400] ??= fn() => "not allowed";
        return $this->errorHandlers[400]();
    }

    public function dispatchNotFound()
    {
        $this->errorHandlers[404] ??= fn() => "not found";
        return $this->errorHandlers[404]();
    }

    public function dispatchError()
    {
        $this->errorHandlers[500] ??= fn() => "server error";
        return $this->errorHandlers[500]();
    }

    public function redirect($path)
    {
        header("Location: {$path}", true, 301);
        exit;
    }
}

