<?php

namespace App;

use Exception;
use App\Routing\Router;

return function(Router $router){
    $router->addRoute('GET', '/',fn()=> 'Hello World');
    $router->addRoute('GET', '/ancien-accueil', fn() => $router->redirect('/'));
    $router->addRoute('GET', '/server-error', fn() => $router->dispatchError);
    $router->addRoute('GET', '/validation-form-error', fn()=> $router->dispatchNotAllowed());
    $router->errorHandler(404, fn() => 'whooops !');
};