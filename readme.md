# Mon mini Router

Un router en php natif sans librairie avec des fonctions basics

##Prérequies

- PHP version >= 8.0

## Installation


Après avoir cloner le projet, installe les indépendances avec composer

```zsh
composer install
```

## Usage

Dans ton index.php qui est le fichier racine de ton application, démarre une instance de Router
```php
<?php

use App\Routing\Router;

require_once('vendor/autoload.php');

$router = new Router();

// chaque route retourne un callable
$routes = require_once __DIR__ . '/src/routes.php';
$routes($router);
echo $router->dispatch();

```

Dans ton dossier src, la classe Route servira de "pattern". Chaque route aura une méthode, un chemin et un callable (le handler). Le callable va appeler une fonction qui affichera la page appelé par la route.

La function matches va vérifier si les méthodes et les chemins sont bien les mêmes. La function dispatch va faire appelle au callable handler.

```php
<?php

namespace App\Routing;

class Route 
{

    protected string $method;
    protected string $path;
    protected $handler;

    public function __construct(string $method, string $path, callable $handler)
    {
        $this->method = $method;
        $this->path = $path;
        $this->handler = $handler;
    }

    public function path() : string
    {
        return $this->path;
    }

    public function method(): string{
        return $this->path;
    }

    public function dispatch()
    {
        return call_user_func($this->handler);
    }

    /**
     * On vérifie si la méthode et le chemin corespondent bien
     * @return bool
     */
    public function matches(string $method, string $path) : bool
    {
        return $this->method === $method and $this->path === $path;
    }
}
```

Ton Router c'est un peu la class qui va s'occuper de la manière dont vont être gérer les routes. 

On met en place une fonction add qui va rajouter une Route. 

Dans une fonction path, on ajoute tout les chemins possible d'accès. 

Dans une fonction match on va vérifier si la route matche avec un path.

Les fonctions errors Handlers vont permettre de renvoyer des erreurs aux navigateurs avec des codes d'état

```php
<?php

namespace App\Routing;

use App\Routing\Route;
use Throwable;

class Router {

    /**
     * @var array qui va stocker toutes les routes possible d'accès
     */
    protected array $routes;

    /**
     * @var array qui va stocker les erreurs du gestionnaire de route
     */
    protected array $errorHandler = [];

    public function addRoute(string $method, string $path, callable $handler) : Route
    {
        return $this->routes[] = new Route($method, $path, $handler);
    }

    public function errorHandler(int $code, callable $handler)
    {
        $this->errorHandlers[$code] = $handler;
    }

    /**
     * 
     * @return array of $paths
     */
    private function paths() : array
    {
        $paths = [];
        foreach($this->routes as $route){
            $paths[] = $route->path();
        }
        return $paths;
    }

    private function match(string $method, string $path) 
    {
        foreach($this->routes as $route){
            if($route->matches($method, $path)){
                return $route;
            }
        }
    }

    public function dispatch()
    {
        $paths = $this->paths();

        // On définit les variable method et uri
        $requestMethod = $_SERVER['REQUEST_METHOD'] ?? 'GET';
        $requestPath = $_SERVER['REQUEST_URI'] ?? '/';

        // est-ce qu'on a un matching entre la method et le chemin ?
        $matching = $this->match($requestMethod, $requestPath);

        if($matching){
            try{
                return $matching->dispatch();
            } catch (Throwable $e) {
                return $this->dispatchError();
            }
        }

        if(in_array($requestPath, $paths)) {
            return $this->dispatchNotAllowed();
        }
        return $this->dispatchNotFound();
    }

    public function dispatchNotAllowed()
    {
        $this->errorHandlers[400] ??= fn() => "not allowed";
        return $this->errorHandlers[400]();
    }

    public function dispatchNotFound()
    {
        $this->errorHandlers[404] ??= fn() => "not found";
        return $this->errorHandlers[404]();
    }

    public function dispatchError()
    {
        $this->errorHandlers[500] ??= fn() => "server error";
        return $this->errorHandlers[500]();
    }

    public function redirect($path)
    {
        header("Location: {$path}", true, 301);
        exit;
    }
}

```

Enfin, le fichier routes.php va lister l'ensemble des routes en leur renvoyant une réponse (le callable)

```php
<?php

namespace App;

use Exception;
use App\Routing\Router;

return function(Router $router){
    $router->addRoute('GET', '/',fn()=> 'Hello World');
    $router->addRoute('GET', '/ancien-accueil', fn() => $router->redirect('/'));
    $router->addRoute('GET', '/server-error', fn() => $router->dispatchError);
    $router->addRoute('GET', '/validation-form-error', fn()=> $router->dispatchNotAllowed());
    $router->errorHandler(404, fn() => 'whooops !');
};

```
## Lancement

Pour tester ton mini router, lance ton application avec le serveur de php en lançant la commande

```zsh
php -S localhost:8000 -d display_errors=1 
``` 